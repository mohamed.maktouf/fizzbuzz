# fizzbuzz

This is a REST "microservice" Spring Boot JHipster application (technical test for LeBonCoin).

## Assignment

### Exercise: Write a simple fizz-buzz REST server.

The original fizz-buzz consists in writing all numbers from 1 to 100, and just replacing all multiples of 3 by "fizz", all multiples of 5 by "buzz", and all multiples of 15 by "fizzbuzz".

The output would look like this: "1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,fizzbuzz,16,...".

1) Your goal is to implement a web server that will expose a REST API endpoint that:
- Accepts five parameters : three integers int1, int2 and limit, and two strings str1 and str2.
- Returns a list of strings with numbers from 1 to limit, where: all multiples of int1 are replaced by str1, all multiples of int2 are replaced by str2, all multiples of int1 and int2 are replaced by str1str2.


2) The server needs to be:
- Ready for production
- Easy to maintain by other developers


3) Add a statistics endpoint allowing users to know what the most frequent request has been.
This endpoint should:
- Accept no parameter
- Return the parameters corresponding to the most used request, as well as the number of hits for this request"

## Requirements:
- **⚠ You have to add <code>keycloak</code> and <code>jhipster-registry</code> domain in your hosts machine ⚠**:
  ```
  jhipster-registry  127.0.0.1
  keycloak           127.0.0.1
  ```
- docker version 20.10.10 (used version)
- docker compose 2.1.1 (used version)

This application is configured for Service Discovery and Configuration with the JHipster-Registry and for OIDC Auth with Keycloak. On launch, it will refuse to start if it is not able to connect to the JHipster-Registry at [http://jhipster-registry:8761](http://jhipster-registry:8761) and Keycloak at [http://keycloak:9080](http://keycloak:9080).

## Project Structure

- `/src/*` structure follows default Java structure (main for source and test for unit tests and integration tests)
- `/src/main/docker` - Docker configurations for the application and services that the application depends on
- `/postman/LeBonCoinMaktouf.postman_collection.json` - Postman collection to import to test the REST Endpoints.
- `.gitlab-ci.yaml` - CI configuration file.

## Installation

```bash
git clone https://gitlab.com/mohamed.maktouf/fizzbuzz.git
cd fizzbuzz
```

### Run
You can use the prebuilt images on dockerhub with docker-compose :
```bash
docker compose -f src/main/docker/docker-compose.yml up
```

Wait some seconds until <code>fizzbuzz-app</code> is started (the last application to start).

### Build
[OpenAPI-Generator]() is configured for this application. You can generate API code from the `src/main/resources/swagger/api.yml` definition file by running:

```bash
./mvnw generate-sources
```

To build the fat jar and run the tests (unit tests and integration tests):
```bash
./mvnw -Pprod clean verify
```

To built the docker image application with JIB:
```bash
./mvnw -Pprod,api-docs verify jib:dockerBuild
```

## Testing

To launch your application's tests, run:

```
./mvnw verify
```

## Usage

The <code>FizzBuzz</code> application exposes two endpoints:
- /api/fizzbuzz : Using this endpoint will perform a fizzbuzz according the assignement. This endpoint is accessible by an authenticated user.
  The params are passed in the request body with a json object. 
- /api/fizzbuzz/stats : Using this endpoint collect the params of the most used request. This endpoint is only accessible by an administrator.

### Postman
The postman collection is in <code>postman/LeBonCoinMaktouf.postman_collection.json</code>

You can use a regular user to obtain a token and hit <code>/api/fizzbuzz</code> (use simpleUserName and simpleUserPassword variables):
  ![Postman_user_auth](user-postman.png)
  **⚠ Don't forget to save after the token generation, before hitting an endpoint ⚠.**

To access the <code>/api/fizzbuzz/stats</code> (use adminUserName and adminUserPassword variables):
![Postman_admin_auth](admin-postman.png)
**⚠ Don't forget to save after the token generation, before hitting an endpoint ⚠.**

## The FizzBuzz core code:
The util class is located at <code>src/main/java/com/leboncoin/fizzbuzz/web/rest/utils/FizzBuzzUtils</code>:

```java
package com.leboncoin.fizzbuzz.web.rest.utils;

import com.leboncoin.fizzbuzz.domain.FizzBuzz;

import java.util.ArrayList;
import java.util.List;

/**
 * Class utils Fizzbuzz
 *
 * @author Mohamed MAKTOUF BOUHLEL
 */
public class FizzBuzzUtils {

    public static class FizzBuzzEntry {
        private String entry;
        public FizzBuzzEntry(String entry) {
            this.entry = entry;
        }

        public String getEntry() {
            return entry;
        }

        public void setEntry(String entry) {
            this.entry = entry;
        }
    }

    /**
     * Get the calculated fizzBuzz list from the fizzBuzz request.
     *
     * @param fizzBuzz The fizzbuzz request to calculate
     * @return The fizzBuzz calculated list
     */
    public static List<FizzBuzzEntry> fizzBuzzGenerator(FizzBuzz fizzBuzz) {
        List<FizzBuzzEntry> fizzBuzzList = new ArrayList<>();
        for (int i = 1; i <= fizzBuzz.getIntLimit(); i++) {
            if (((i % fizzBuzz.getInt1()) == 0) && ((i % fizzBuzz.getInt2()) == 0))
                fizzBuzzList.add(new FizzBuzzEntry(fizzBuzz.getStr1().concat(fizzBuzz.getStr2())));
            else if ((i % fizzBuzz.getInt1()) == 0)
                fizzBuzzList.add(new FizzBuzzEntry(fizzBuzz.getStr1()));
            else if ((i % fizzBuzz.getInt2()) == 0)
                fizzBuzzList.add(new FizzBuzzEntry(fizzBuzz.getStr2()));
            else
                fizzBuzzList.add(new FizzBuzzEntry(String.valueOf(i)));
        }
        return fizzBuzzList;
    }
}
```

## Monitoring
You can access to the monitoring through [jhipster-registry](http://jhipster-registry:8761) (default login/pass admin/admin).

## Areas for improvement
- Using a solution like Hashicorp Vault to store the key/value and to use the database tool to rotate the databases credentials;
- Using a backup solution like VmWare Velero;
- Using an orchestrator like Kubernetes with prometheus/grafana/alertmanager to monitor the application.

## Licence
[MIT](LICENCE)
