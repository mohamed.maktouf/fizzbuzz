package com.leboncoin.fizzbuzz.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.leboncoin.fizzbuzz.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class FizzBuzzTest {

    @Test
    void equalsVerifier() throws Exception {
        FizzBuzz fizzBuzz1 = new FizzBuzz();
        fizzBuzz1.setId(1L);
        fizzBuzz1.setInt1(1);
        fizzBuzz1.setInt2(2);
        fizzBuzz1.setStr1("str1");
        fizzBuzz1.setStr2("str2");
        FizzBuzz fizzBuzz2 = new FizzBuzz();
        fizzBuzz1.setId(2L);
        fizzBuzz1.setInt1(2);
        fizzBuzz1.setInt2(3);
        fizzBuzz1.setStr1("str1");
        fizzBuzz1.setStr2("str2");
        assertThat(fizzBuzz1).isNotEqualTo(fizzBuzz2);
    }
}
