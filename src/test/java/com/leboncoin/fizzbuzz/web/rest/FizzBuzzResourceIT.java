package com.leboncoin.fizzbuzz.web.rest;

import com.leboncoin.fizzbuzz.IntegrationTest;
import com.leboncoin.fizzbuzz.domain.FizzBuzz;
import com.leboncoin.fizzbuzz.repository.FizzBuzzRepository;
import com.leboncoin.fizzbuzz.security.AuthoritiesConstants;
import com.leboncoin.fizzbuzz.web.rest.utils.FizzBuzzUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.leboncoin.fizzbuzz.web.rest.utils.FizzBuzzUtils.fizzBuzzGenerator;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FizzBuzzResource} REST controller.
 *
 * @author Mohamed MAKTOUF BOUHLEL
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class FizzBuzzResourceIT {

    private static final Integer DEFAULT_INT_1 = 3;

    private static final Integer UPDATED_INT_1 = 2;

    private static final Integer DEFAULT_INT_2 = 7;

    private static final Integer UPDATED_INT_2 = 5;

    private static final String DEFAULT_STR_1 = "fizz";

    private static final String DEFAULT_STR_2 = "Buzz";

    private static final Integer DEFAULT_INT_LIMIT = 10;

    private static final String ENTITY_API_URL = "/api/fizzbuzz";

    private static final String ENTITY_API_STATS_URL = "/api/fizzbuzz/stats";

    @Autowired
    private FizzBuzzRepository fizzBuzzRepository;

    @Autowired
    private MockMvc restFizzBuzzMockMvc;

    private FizzBuzz fizzBuzz;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FizzBuzz createEntity() {
        return new FizzBuzz()
            .int1(DEFAULT_INT_1)
            .int2(DEFAULT_INT_2)
            .str1(DEFAULT_STR_1)
            .str2(DEFAULT_STR_2)
            .intLimit(DEFAULT_INT_LIMIT);
    }

    @BeforeEach
    public void initTest() {
        fizzBuzz = createEntity();
    }

    @Test
    @WithMockUser(authorities = AuthoritiesConstants.ADMIN)
    @Transactional
    void createFizzBuzz() throws Exception {
        int databaseSizeBeforeCreate = fizzBuzzRepository.findAll().size();

        // Expected result of FizzBuzz
        List<FizzBuzzUtils.FizzBuzzEntry> expectedFizzBuzz = fizzBuzzGenerator(fizzBuzz);

        // First hit of the first type request
        restFizzBuzzMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(fizzBuzz))
            )
            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].entry").value(expectedFizzBuzz.get(0).getEntry()))
            .andExpect(jsonPath("$.[1].entry").value(expectedFizzBuzz.get(1).getEntry()))
            .andExpect(jsonPath("$.[2].entry").value(expectedFizzBuzz.get(2).getEntry()))
            .andExpect(jsonPath("$.[3].entry").value(expectedFizzBuzz.get(3).getEntry()))
            .andExpect(jsonPath("$.[4].entry").value(expectedFizzBuzz.get(4).getEntry()))
            .andExpect(jsonPath("$.[5].entry").value(expectedFizzBuzz.get(5).getEntry()))
            .andExpect(jsonPath("$.[6].entry").value(expectedFizzBuzz.get(6).getEntry()))
            .andExpect(jsonPath("$.[7].entry").value(expectedFizzBuzz.get(7).getEntry()))
            .andExpect(jsonPath("$.[8].entry").value(expectedFizzBuzz.get(8).getEntry()))
            .andExpect(jsonPath("$.[9].entry").value(expectedFizzBuzz.get(9).getEntry()));

        // Second hit of the first type request
        restFizzBuzzMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(fizzBuzz))
            )
            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].entry").value(expectedFizzBuzz.get(0).getEntry()))
            .andExpect(jsonPath("$.[1].entry").value(expectedFizzBuzz.get(1).getEntry()))
            .andExpect(jsonPath("$.[2].entry").value(expectedFizzBuzz.get(2).getEntry()))
            .andExpect(jsonPath("$.[3].entry").value(expectedFizzBuzz.get(3).getEntry()))
            .andExpect(jsonPath("$.[4].entry").value(expectedFizzBuzz.get(4).getEntry()))
            .andExpect(jsonPath("$.[5].entry").value(expectedFizzBuzz.get(5).getEntry()))
            .andExpect(jsonPath("$.[6].entry").value(expectedFizzBuzz.get(6).getEntry()))
            .andExpect(jsonPath("$.[7].entry").value(expectedFizzBuzz.get(7).getEntry()))
            .andExpect(jsonPath("$.[8].entry").value(expectedFizzBuzz.get(8).getEntry()))
            .andExpect(jsonPath("$.[9].entry").value(expectedFizzBuzz.get(9).getEntry()));

        String firstFizzBuzzHash = fizzBuzz.getHash();
        // Second request type
        fizzBuzz.setInt1(UPDATED_INT_1);
        fizzBuzz.setInt2(UPDATED_INT_2);

        // Expected result of the second FizzBuzz
        expectedFizzBuzz = fizzBuzzGenerator(fizzBuzz);

        // One hit of the second request type
        restFizzBuzzMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(fizzBuzz))
            )
            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[0].entry").value(expectedFizzBuzz.get(0).getEntry()))
            .andExpect(jsonPath("$.[1].entry").value(expectedFizzBuzz.get(1).getEntry()))
            .andExpect(jsonPath("$.[2].entry").value(expectedFizzBuzz.get(2).getEntry()))
            .andExpect(jsonPath("$.[3].entry").value(expectedFizzBuzz.get(3).getEntry()))
            .andExpect(jsonPath("$.[4].entry").value(expectedFizzBuzz.get(4).getEntry()))
            .andExpect(jsonPath("$.[5].entry").value(expectedFizzBuzz.get(5).getEntry()))
            .andExpect(jsonPath("$.[6].entry").value(expectedFizzBuzz.get(6).getEntry()))
            .andExpect(jsonPath("$.[7].entry").value(expectedFizzBuzz.get(7).getEntry()))
            .andExpect(jsonPath("$.[8].entry").value(expectedFizzBuzz.get(8).getEntry()))
            .andExpect(jsonPath("$.[9].entry").value(expectedFizzBuzz.get(9).getEntry()));

        // Validate the FizzBuzz in the database
        List<FizzBuzz> fizzBuzzList = fizzBuzzRepository.findAll();

        // Only 2 requests are created in database (one with 2 hits and another with one hit)
        assertThat(fizzBuzzList).hasSize(databaseSizeBeforeCreate + 2);

        FizzBuzz testFizzBuzz = fizzBuzzList.get(fizzBuzzList.size() - 2);
        assertThat(testFizzBuzz.getInt1()).isEqualTo(DEFAULT_INT_1);
        assertThat(testFizzBuzz.getInt2()).isEqualTo(DEFAULT_INT_2);
        assertThat(testFizzBuzz.getIntLimit()).isEqualTo(DEFAULT_INT_LIMIT);
        assertThat(testFizzBuzz.getStr1()).isEqualTo(DEFAULT_STR_1);
        assertThat(testFizzBuzz.getStr2()).isEqualTo(DEFAULT_STR_2);
        assertThat(testFizzBuzz.getHits()).isEqualTo(2L); // two hits on this request
        assertThat(testFizzBuzz.getHash()).isEqualTo(firstFizzBuzzHash);

        testFizzBuzz = fizzBuzzList.get(fizzBuzzList.size() - 1);
        assertThat(testFizzBuzz.getInt1()).isEqualTo(UPDATED_INT_1);
        assertThat(testFizzBuzz.getInt2()).isEqualTo(UPDATED_INT_2);
        assertThat(testFizzBuzz.getIntLimit()).isEqualTo(DEFAULT_INT_LIMIT);
        assertThat(testFizzBuzz.getStr1()).isEqualTo(DEFAULT_STR_1);
        assertThat(testFizzBuzz.getStr2()).isEqualTo(DEFAULT_STR_2);
        assertThat(testFizzBuzz.getHits()).isEqualTo(1L); // only one hit on this request
        assertThat(testFizzBuzz.getHash()).isEqualTo(fizzBuzz.getHash());

        //----------- Stats request --------------//
        restFizzBuzzMockMvc
            .perform(get(ENTITY_API_STATS_URL))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.int1").value(DEFAULT_INT_1))
            .andExpect(jsonPath("$.int2").value(DEFAULT_INT_2))
            .andExpect(jsonPath("$.intLimit").value(DEFAULT_INT_LIMIT))
            .andExpect(jsonPath("$.str1").value(DEFAULT_STR_1))
            .andExpect(jsonPath("$.str2").value(DEFAULT_STR_2))
            .andExpect(jsonPath("$.hits").value(2L));
    }

    @Test
    @Transactional
    void createFizzBuzzWithExistingId() throws Exception {
        // Create the FizzBuzz with an existing ID
        fizzBuzz.setId(1L);

        int databaseSizeBeforeCreate = fizzBuzzRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restFizzBuzzMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(fizzBuzz))
            )
            .andExpect(status().isBadRequest());

        // Validate the FizzBuzz in the database
        List<FizzBuzz> fizzBuzzList = fizzBuzzRepository.findAll();
        assertThat(fizzBuzzList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkInt1IsRequired() throws Exception {
        int databaseSizeBeforeTest = fizzBuzzRepository.findAll().size();
        // set the field null
        fizzBuzz.setInt1(null);

        // Create the FizzBuzz, which fails.

        restFizzBuzzMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(fizzBuzz))
            )
            .andExpect(status().isBadRequest());

        List<FizzBuzz> fizzBuzzList = fizzBuzzRepository.findAll();
        assertThat(fizzBuzzList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkInt2IsRequired() throws Exception {
        int databaseSizeBeforeTest = fizzBuzzRepository.findAll().size();
        // set the field null
        fizzBuzz.setInt2(null);

        // Create the FizzBuzz, which fails.

        restFizzBuzzMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(fizzBuzz))
            )
            .andExpect(status().isBadRequest());

        List<FizzBuzz> fizzBuzzList = fizzBuzzRepository.findAll();
        assertThat(fizzBuzzList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkIntLimitIsRequired() throws Exception {
        int databaseSizeBeforeTest = fizzBuzzRepository.findAll().size();
        // set the field null
        fizzBuzz.setIntLimit(null);

        // Create the FizzBuzz, which fails.

        restFizzBuzzMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(fizzBuzz))
            )
            .andExpect(status().isBadRequest());

        List<FizzBuzz> fizzBuzzList = fizzBuzzRepository.findAll();
        assertThat(fizzBuzzList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkStr1IsRequired() throws Exception {
        int databaseSizeBeforeTest = fizzBuzzRepository.findAll().size();
        // set the field null
        fizzBuzz.setStr1(null);

        // Create the FizzBuzz, which fails.

        restFizzBuzzMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(fizzBuzz))
            )
            .andExpect(status().isBadRequest());

        List<FizzBuzz> fizzBuzzList = fizzBuzzRepository.findAll();
        assertThat(fizzBuzzList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkStr2IsRequired() throws Exception {
        int databaseSizeBeforeTest = fizzBuzzRepository.findAll().size();
        // set the field null
        fizzBuzz.setStr2(null);

        // Create the FizzBuzz, which fails.

        restFizzBuzzMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(fizzBuzz))
            )
            .andExpect(status().isBadRequest());

        List<FizzBuzz> fizzBuzzList = fizzBuzzRepository.findAll();
        assertThat(fizzBuzzList).hasSize(databaseSizeBeforeTest);
    }
}
