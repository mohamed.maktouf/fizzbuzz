package com.leboncoin.fizzbuzz.repository;

import com.leboncoin.fizzbuzz.domain.FizzBuzz;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data SQL repository for the FizzBuzz entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FizzBuzzRepository extends JpaRepository<FizzBuzz, Long> {

    /**
     * Get the most requested {@link FizzBuzz}
     *
     * @return the entity.
     */
//    @Query("SELECT f FROM FizzBuzz f ORDER BY f.hits LIMIT 1")
    Optional<FizzBuzz> findFirstByOrderByHitsDesc();

    /**
     * Get a {@link FizzBuzz} by his hash value.
     *
     * @param hash The fizzBuzz hash
     * @return An optional fizzBuzz
     */
    Optional<FizzBuzz> findFizzBuzzByHash(String hash);
}
