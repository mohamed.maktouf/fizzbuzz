package com.leboncoin.fizzbuzz.service;

import com.leboncoin.fizzbuzz.domain.FizzBuzz;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link FizzBuzz}.
 */
public interface FizzBuzzService {
    /**
     * Save a fizzBuzz.
     *
     * @param fizzBuzz the entity to save.
     * @return the persisted entity.
     */
    FizzBuzz save(FizzBuzz fizzBuzz);

    /**
     * Get the most requested {@link FizzBuzz}
     *
     * @return the entity.
     */
    Optional<FizzBuzz> findFirstByOrderByHitsDesc();

    /**
     * Get a {@link FizzBuzz} by his hash value.
     *
     * @param hash The fizzBuzz hash
     * @return An optional fizzBuzz
     */
    Optional<FizzBuzz> findFizzBuzzByHash(String hash);
}
