package com.leboncoin.fizzbuzz.service.impl;

import com.leboncoin.fizzbuzz.domain.FizzBuzz;
import com.leboncoin.fizzbuzz.repository.FizzBuzzRepository;
import com.leboncoin.fizzbuzz.service.FizzBuzzService;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link FizzBuzz}.
 */
@Service
@Transactional
public class FizzBuzzServiceImpl implements FizzBuzzService {

    private final Logger log = LoggerFactory.getLogger(FizzBuzzServiceImpl.class);

    private final FizzBuzzRepository fizzBuzzRepository;

    public FizzBuzzServiceImpl(FizzBuzzRepository fizzBuzzRepository) {
        this.fizzBuzzRepository = fizzBuzzRepository;
    }

    @Override
    public FizzBuzz save(FizzBuzz fizzBuzz) {
        log.debug("Request to save FizzBuzz : {}", fizzBuzz);
        return fizzBuzzRepository.save(fizzBuzz);
    }

    /**
     * Get the most requested {@link FizzBuzz}
     *
     * @return the entity.
     */
    @Override
    public Optional<FizzBuzz> findFirstByOrderByHitsDesc() {
        log.debug("Request to get the most requested FizzBuzz");
        return fizzBuzzRepository.findFirstByOrderByHitsDesc();
    }

    /**
     * Get a {@link FizzBuzz} by his hash value.
     *
     * @param hash The fizzBuzz hash
     * @return An optional fizzBuzz
     */
    @Override
    public Optional<FizzBuzz> findFizzBuzzByHash(String hash) {
        log.debug("Request to get FizzBuzz by his hash : {}", hash);
        return fizzBuzzRepository.findFizzBuzzByHash(hash);
    }
}
