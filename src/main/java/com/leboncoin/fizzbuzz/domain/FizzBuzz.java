package com.leboncoin.fizzbuzz.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

/**
 * The FizzBuzz entity.
 *
 * @author Mohamed MAKTOUF BOUHLEL
 */
@Schema(description = "The FizzBuzz entity.\n\n@author Mohamed MAKTOUF BOUHLEL")
@Entity
@Table(name = "fizz_buzz")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class FizzBuzz implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Entity id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fizzbuzzSequenceGenerator")
    @SequenceGenerator(name = "fizzbuzzSequenceGenerator", allocationSize = 1)
    @Column(name = "id")
    private Long id;

    /**
     * First int multiple
     */
    @Schema(description = "First int multiple", required = true)
    @NotNull
    @Column(name = "int_1", nullable = false)
    private Integer int1;

    /**
     * Second int multiple
     */
    @Schema(description = "Second int multiple", required = true)
    @NotNull
    @Column(name = "int_2", nullable = false)
    private Integer int2;

    /**
     * The limit value for the loop to generate the fizzbuzz output
     */
    @Schema(description = "The limit value for the loop to generate the fizzbuzz output", required = true)
    @NotNull
    @Min(value = 1)
    @Max(value = 100)
    @Column(name = "int_limit", nullable = false)
    private Integer intLimit;

    /**
     * The value to use to replace the number when it is a multiple of <code>int1</code>
     */
    @Schema(description = "The value to use to replace the number when it is a multiple of <code>int1</code>", required = true)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "str_1", length = 10, nullable = false)
    private String str1;

    /**
     * The value to use to replace the number when it is a multiple of <code>int2</code>
     */
    @Schema(description = "The value to use to replace the number when it is a multiple of <code>int2</code>", required = true)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "str_2", length = 10, nullable = false)
    private String str2;

    /**
     * The number of times a fizzbuzz request was sent
     */
    @Schema(description = "The number of times a fizzbuzz request was sent", required = true)
    @NotNull
    @Column(name = "hits", nullable = false, columnDefinition = "default 1")
    private Long hits = 1L;

    /**
     * A hash value to easily find an existing request in database
     * <p>
     * Json ignore the hash (for internal use)
     */
    @Schema(description = "A hash value to easily find an existing request in database", required = true)
    @Size(min = 64, max = 64)
    @Column(name = "hash", length = 64, nullable = false, unique = true)
    @JsonIgnore
    private String hash;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public FizzBuzz id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getInt1() {
        return this.int1;
    }

    public FizzBuzz int1(Integer int1) {
        this.setInt1(int1);
        return this;
    }

    public void setInt1(Integer int1) {
        this.int1 = int1;
    }

    public Integer getInt2() {
        return this.int2;
    }

    public FizzBuzz int2(Integer int2) {
        this.setInt2(int2);
        return this;
    }

    public void setInt2(Integer int2) {
        this.int2 = int2;
    }

    public Integer getIntLimit() {
        return this.intLimit;
    }

    public FizzBuzz intLimit(Integer intLimit) {
        this.setIntLimit(intLimit);
        return this;
    }

    public void setIntLimit(Integer intLimit) {
        this.intLimit = intLimit;
    }

    public String getStr1() {
        return this.str1;
    }

    public FizzBuzz str1(String str1) {
        this.setStr1(str1);
        return this;
    }

    public void setStr1(String str1) {
        this.str1 = str1;
    }

    public String getStr2() {
        return this.str2;
    }

    public FizzBuzz str2(String str2) {
        this.setStr2(str2);
        return this;
    }

    public void setStr2(String str2) {
        this.str2 = str2;
    }

    public Long getHits() {
        return this.hits;
    }

    public FizzBuzz hits(Long hits) {
        this.setHits(hits);
        return this;
    }

    public void setHits(Long hits) {
        this.hits = hits;
    }

    public String getHash() throws NoSuchAlgorithmException {
        this.hash = this.getDigest();
        return hash;
    }

    public FizzBuzz hash(String hash) {
        this.setHash(hash);
        return this;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    /**
     * Get the sha-256 hash from the current object
     *
     * @return The SHA-256 digest in Hex format.
     * @throws NoSuchAlgorithmException Fail to load the {@link MessageDigest} instance.
     */
    private String getDigest() throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        return toHexString(digest.digest(this.toString().getBytes(StandardCharsets.UTF_8)));
    }

    /**
     * Get the Hex value from <code>bytes</code>
     *
     * @param bytes Value to process.
     * @return The Hex value.
     */
    private static String toHexString(byte[] bytes) {
        StringBuilder digestHex = new StringBuilder();
        for (byte aByte : bytes) {
            String hex = Integer.toHexString(0xFF & aByte);
            if (hex.length() == 1) {
                digestHex.append('0');
            }
            digestHex.append(hex);
        }
        return digestHex.toString().toUpperCase();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FizzBuzz fizzBuzz = (FizzBuzz) o;
        return Objects.equals(int1, fizzBuzz.int1) && Objects.equals(int2, fizzBuzz.int2) && Objects.equals(intLimit, fizzBuzz.intLimit) && Objects.equals(str1, fizzBuzz.str1) && Objects.equals(str2, fizzBuzz.str2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(int1, int2, intLimit, str1, str2);
    }

    @Override
    public String toString() {
        return "FizzBuzz{" +
            "int1=" + int1 +
            ", int2=" + int2 +
            ", intLimit=" + intLimit +
            ", str1='" + str1 + '\'' +
            ", str2='" + str2 + '\'' +
            '}';
    }
}
