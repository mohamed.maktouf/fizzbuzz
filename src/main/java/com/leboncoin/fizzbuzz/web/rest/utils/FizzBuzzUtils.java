package com.leboncoin.fizzbuzz.web.rest.utils;

import com.leboncoin.fizzbuzz.domain.FizzBuzz;

import java.util.ArrayList;
import java.util.List;

/**
 * Class utils Fizzbuzz
 *
 * @author Mohamed MAKTOUF BOUHLEL
 */
public class FizzBuzzUtils {

    public static class FizzBuzzEntry {
        private String entry;
        public FizzBuzzEntry(String entry) {
            this.entry = entry;
        }

        public String getEntry() {
            return entry;
        }

        public void setEntry(String entry) {
            this.entry = entry;
        }
    }

    /**
     * Get the calculated fizzBuzz list from the fizzBuzz request.
     *
     * @param fizzBuzz The fizzbuzz request to calculate
     * @return The fizzBuzz calculated list
     */
    public static List<FizzBuzzEntry> fizzBuzzGenerator(FizzBuzz fizzBuzz) {
        List<FizzBuzzEntry> fizzBuzzList = new ArrayList<>();
        for (int i = 1; i <= fizzBuzz.getIntLimit(); i++) {
            if (((i % fizzBuzz.getInt1()) == 0) && ((i % fizzBuzz.getInt2()) == 0))
                fizzBuzzList.add(new FizzBuzzEntry(fizzBuzz.getStr1().concat(fizzBuzz.getStr2())));
            else if ((i % fizzBuzz.getInt1()) == 0)
                fizzBuzzList.add(new FizzBuzzEntry(fizzBuzz.getStr1()));
            else if ((i % fizzBuzz.getInt2()) == 0)
                fizzBuzzList.add(new FizzBuzzEntry(fizzBuzz.getStr2()));
            else
                fizzBuzzList.add(new FizzBuzzEntry(String.valueOf(i)));
        }
        return fizzBuzzList;
    }
}
