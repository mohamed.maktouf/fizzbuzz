/**
 * View Models used by Spring MVC REST controllers.
 */
package com.leboncoin.fizzbuzz.web.rest.vm;
