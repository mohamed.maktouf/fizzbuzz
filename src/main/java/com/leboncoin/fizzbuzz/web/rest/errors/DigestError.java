package com.leboncoin.fizzbuzz.web.rest.errors;

/**
 * A class exception to thrown at request digest calculation.
 *
 * @author - Mohamed MAKTOUF BOUHLEL
 */
public class DigestError extends ServerError {
    public DigestError(String defaultMessage, String entityName, String errorKey) {
        super(defaultMessage, entityName, errorKey);
    }
}
