package com.leboncoin.fizzbuzz.web.rest.errors;

import org.zalando.problem.Status;

/**
 * A class exception to thrown at server problem.
 *
 * @author - Mohamed MAKTOUF BOUHLEL
 */
public class ServerError extends AbstractCustomException {

    public ServerError(String defaultMessage, String entityName, String errorKey) {
        super(null, defaultMessage, entityName, errorKey, Status.INTERNAL_SERVER_ERROR);
    }
}
