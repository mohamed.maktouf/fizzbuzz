package com.leboncoin.fizzbuzz.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Mohamed MAKTOUF BOUHLEL
 */
public class AbstractCustomException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;

    private final String entityName;

    private final String errorKey;

    public AbstractCustomException(String defaultMessage, String entityName, String errorKey, Status status) {
        this(ErrorConstants.DEFAULT_TYPE, defaultMessage, entityName, errorKey, status);
    }

    public AbstractCustomException(URI type, String defaultMessage, String entityName, String errorKey, Status status) {
        super(type, defaultMessage, status, null, null, null, getAlertParameters(entityName, errorKey));
        this.entityName = entityName;
        this.errorKey = errorKey;
    }

    public String getEntityName() {
        return entityName;
    }

    public String getErrorKey() {
        return errorKey;
    }

    protected static Map<String, Object> getAlertParameters(String entityName, String errorKey) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("message", "error." + errorKey);
        parameters.put("params", entityName);
        return parameters;
    }
}
