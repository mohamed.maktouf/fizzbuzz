package com.leboncoin.fizzbuzz.web.rest;

import com.leboncoin.fizzbuzz.domain.FizzBuzz;
import com.leboncoin.fizzbuzz.security.AuthoritiesConstants;
import com.leboncoin.fizzbuzz.service.FizzBuzzService;
import com.leboncoin.fizzbuzz.web.rest.errors.BadRequestAlertException;
import com.leboncoin.fizzbuzz.web.rest.errors.DigestError;
import com.leboncoin.fizzbuzz.web.rest.utils.FizzBuzzUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;

import static com.leboncoin.fizzbuzz.web.rest.utils.FizzBuzzUtils.fizzBuzzGenerator;

/**
 * REST controller for managing {@link com.leboncoin.fizzbuzz.domain.FizzBuzz}.
 */
@RestController
@RequestMapping("/api")
public class FizzBuzzResource {

    private final Logger log = LoggerFactory.getLogger(FizzBuzzResource.class);

    private static final String ENTITY_NAME = "fizzbuzz";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FizzBuzzService fizzBuzzService;

    public FizzBuzzResource(FizzBuzzService fizzBuzzService) {
        this.fizzBuzzService = fizzBuzzService;
    }

    /**
     * {@code POST  /fizzbuzz} : Create a new fizzBuzz.
     *
     * @param fizzBuzz the fizzBuzz to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fizzBuzz, or with status {@code 400 (Bad Request)} if the fizzBuzz has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/fizzbuzz")
    public ResponseEntity<List<FizzBuzzUtils.FizzBuzzEntry>> createFizzBuzz(@Valid @RequestBody FizzBuzz fizzBuzz) throws URISyntaxException {
        log.debug("REST request to save FizzBuzz : {}", fizzBuzz);
        if (fizzBuzz.getId() != null) {
            throw new BadRequestAlertException("A new fizzBuzz cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Optional<FizzBuzz> existingFizzbuzz;
        try {
            existingFizzbuzz = fizzBuzzService.findFizzBuzzByHash(fizzBuzz.getHash());
        } catch (NoSuchAlgorithmException e) {
            throw new DigestError("A server error occured", ENTITY_NAME, "digestcalculationerror-createFizzBuzz");
        }

        if (existingFizzbuzz.isPresent()) {
            fizzBuzz = existingFizzbuzz.get();
            fizzBuzz.setHits(fizzBuzz.getHits() + 1);
        }

        FizzBuzz result = fizzBuzzService.save(fizzBuzz);
        return ResponseEntity
            .created(new URI("/api/fizzbuzz/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(fizzBuzzGenerator(fizzBuzz));
    }

    /**
     * {@code GET  /fizzbuzz} : get the most requested fizzBuzz.
     * <strong>Only accessible by administrators.</strong>
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fizzBuzz, or with status {@code 404 (Not Found)}.
     */
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    @GetMapping("/fizzbuzz/stats")
    public ResponseEntity<FizzBuzz> getFizzBuzz() {
        log.debug("REST request to get the most requested");
        return ResponseUtil.wrapOrNotFound(fizzBuzzService.findFirstByOrderByHitsDesc());
    }
}
